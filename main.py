import sys

chunk = 3000  # number of lines from the big file to put in small file
source_file = sys.argv[1] if len(sys.argv) >= 2 else 'competitor_mappings.csv'

source_filename_split = source_file.split('.')
base_name = "".join(source_filename_split[:-1])
extension = source_filename_split[-1]

with open(source_file) as file_to_read:
    all_lines = file_to_read.readlines()

    file_index_count = 0
    while True:
        lines_to_write = all_lines[:chunk]
        del all_lines[:chunk]

        if not lines_to_write:
            break;

        file_name = f'{base_name}_{file_index_count}.{extension}'
        print(f"Writing {len(lines_to_write)} lines to file {file_name}")
        with open(file_name, 'w+') as small_file:
            small_file.writelines(lines_to_write)

        file_index_count += 1
